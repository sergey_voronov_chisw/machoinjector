//
//  MachOUtilities.c
//  MachOInjector
//
//  Created by userMacBookPro on 7/18/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

#include "MachOUtilities.h"
//#include <mach-o/loader.h>
#include <mach-o/fat.h>
#include <memory.h> // for memcpy()
#include <stdlib.h> // for calloc()
#include <stdio.h>  // for sprintf()
#include <errno.h>

//#include <stdio.h>
//#include <stdlib.h>
#include <unistd.h> // for open(), lseek(), write(), _exit()
//#include <string.h>
#include <fcntl.h> // for O_RDONLY, O_RDWR, O_CREAT, O_TRUNC
//#include <mach-o/fat.h>


//namespace macho {

uint32_t macho_read_magic(const char *pBuf /*FILE *obj_file*/, int offset/* = 0*/);

//--------------------------------------------------------------------------------------------------

uint32_t macho_read_magic(const char *pBuf/* FILE *obj_file*/, int offset) {
    uint32_t magic;
//    fseek(obj_file, offset, SEEK_SET);
//    fread(&magic, sizeof(uint32_t), 1, obj_file);
    const char *sourceBuf = pBuf + offset;
    magic = *(uint32_t *)sourceBuf;
    return magic;
}

//--------------------------------------------------------------------------------------------------

int macho_is_magic_64(uint32_t magic) /*throw(int)*/ {
    int result = 0;
    switch (magic) {
        case MH_MAGIC_64:
        case MH_CIGAM_64:
            result = 1;
            break;
        case MH_MAGIC:
        case MH_CIGAM:
            break;
        default:
            result = -1;
            break;
    }
    return result;
}

//--------------------------------------------------------------------------------------------------

int macho_should_swap_bytes(uint32_t magic) {
    return magic == MH_CIGAM || magic == MH_CIGAM_64 || magic == FAT_CIGAM;
}

//--------------------------------------------------------------------------------------------------

int macho_is_fat(uint32_t magic) {
    return magic == FAT_MAGIC || magic == FAT_CIGAM;
}

//--------------------------------------------------------------------------------------------------

//void *load_bytes(const char *pBuf, // FILE *obj_file
//                 int offset, int size) {
//    void *buf = calloc(1, size);
////    fseek(obj_file, offset, SEEK_SET);
////    fread(buf, size, 1, obj_file);
//    const char *sourceBuf = pBuf + offset;
//    memcpy(buf, sourceBuf, size);
//    
//    return buf;
//}

void* macho_copy_bytes(const char *pBuf, int size, int offset) {
//    void *buf = calloc(1, size);
    void *result = NULL;
//    const char *sourceBuf = pBuf + offset;
    memcpy(result, pBuf + offset, size);
    return result;
}

//--------------------------------------------------------------------------------------------------

char* macho_combinePatchFileNameWithLocalPath(const char *patchFileName, char *machORPath) {
    
    //    char *machORPath = spParser->ReadMachORPath();
    printf("rpath: %s\n", machORPath);
    const char *path = machORPath ? "@rpath" : "@executable_path/Frameworks";
    printf("path : %s\n", path);
    size_t len = strlen(path) + 1 + strlen(patchFileName);
    char *patchFileWithPath = (char *)calloc(1, len + 1);
    sprintf(patchFileWithPath, "%s/%s", path, patchFileName);
    printf("patch: %s\n", patchFileWithPath);
    free(machORPath);
    return patchFileWithPath;
}

//--------------------------------------------------------------------------------------------------

#define swap32(value) (((value & 0xFF000000) >> 24) | ((value & 0x00FF0000) >> 8) | ((value & 0x0000FF00) << 8) | ((value & 0x000000FF) << 24) )

//--------------------------------------------------------------------------------------------------

int macho_copyDataFromSrcFileToDestFile(int srcFileHandle, int destFileHandle, char *buffer, int bytesCount) {
    
    int result = 0;
    
    int bytesToRead, bytesRead;
    
    while (bytesCount > 0) {
        bytesToRead = (bytesCount > sizeof(buffer)) ? sizeof(buffer) : bytesCount;
        bytesRead = (int)read(srcFileHandle, buffer, bytesToRead);
        if (bytesRead != bytesToRead) {
            printf("[-] Error reading file\n");
            result = 0; // _exit(1);
            break;
        }
        bytesCount -= bytesRead;
        
        bytesRead = (int)write(destFileHandle, buffer, bytesToRead);
        if (bytesRead != bytesToRead) {
            printf("[-] Error writing file\n");
            result = 0; // _exit(1);
            break;
        }
    }
    
    return result;
}

//--------------------------------------------------------------------------------------------------

//__attribute__((constructor))
void macho_decrypt(const char *thisFileName, /*int argc, const char **argv, const char **envp, const char **apple, struct ProgramVars *pvars*/struct mach_header *machHdr) {
    
    struct load_command            *loadCmd;
    struct encryption_info_command *encInfoCmd;
    struct fat_header              *fatHdr;
    struct fat_arch                *arch;
    //    struct mach_header             *machHdr;
    
    char buffer[1024];
    char rPath[4096], npath[4096]; /* should be big enough for PATH_MAX */
    unsigned int fileoffs = 0, cryptIdOffset = 0, restSize;
    int i, srcFileHandle, destFileHandle, res, bytesCount/*, bytesToRead*/;
    char *tmp;
    
    printf("mach-o decryption dumper\n\n");
    
    printf("DISCLAIMER: This tool is only meant for security research purposes, not for application crackers.\n\n");
    
    
    /* detect if this is a arm64 binary */
    
    if (/*pvars->*/machHdr->magic == MH_MAGIC_64) {
        loadCmd = (struct load_command *)((unsigned char *)/*pvars->*/machHdr + sizeof(struct mach_header_64));
        printf("[+] detected 64bit ARM binary in memory.\n");
    } else { /* we might want to check for other errors here, too */
        loadCmd = (struct load_command *)((unsigned char *)machHdr + sizeof(struct mach_header));
        printf("[+] detected 32bit ARM binary in memory.\n");
    }
    
    
    /* searching all load commands for an LC_ENCRYPTION_INFO load command */
    
    for (i=0; i</*pvars->*/machHdr->ncmds; i++) {
        /*printf("Load Command (%d): %08x\n", i, lc->cmd);*/
        
        if (loadCmd->cmd == LC_ENCRYPTION_INFO || loadCmd->cmd == LC_ENCRYPTION_INFO_64) {
            
            encInfoCmd = (struct encryption_info_command *)loadCmd;
            
            
            /* If this load command is present, but data is not crypted then exit */
            
            if (encInfoCmd->cryptid == 0) {
                break;
            }
//            cryptIdOffset = (/*off_t*/unsigned)((void*)&encInfoCmd->cryptid - (void*)machHdr);
            printf("[+] offset to cryptid found: @%p(from %p) = %x\n", &encInfoCmd->cryptid, machHdr, cryptIdOffset);
            
            printf("[+] Found encrypted data at address %08x of length %u bytes - type %u.\n",
                   encInfoCmd->cryptoff, encInfoCmd->cryptsize, encInfoCmd->cryptid);
            
            if (realpath(thisFileName/*argv[0]*/, rPath) == NULL) {
                strlcpy(rPath, thisFileName/*argv[0]*/, sizeof(rPath));
            }
            
            printf("[+] Opening %s for reading.\n", rPath);
            srcFileHandle = open(rPath, O_RDONLY);
            if (srcFileHandle == -1) {
                printf("[-] Failed opening.\n");
                return; // _exit(1);
            }
            
            printf("[+] Reading header\n");
            bytesCount = (int)read(srcFileHandle, (void *)buffer, sizeof(buffer));
            if (bytesCount != sizeof(buffer)) {
                printf("[W] Warning read only %d bytes\n", bytesCount);
            }
            
            printf("[+] Detecting header type\n");
            fatHdr = (struct fat_header *)buffer;
            
            
            /* Is this a FAT file - we assume the right endianess */
            
            if (fatHdr->magic == FAT_CIGAM) {
                printf("[+] Executable is a FAT image - searching for right architecture\n");
                arch = (struct fat_arch *)&fatHdr[1];
                for (i=0; i<swap32(fatHdr->nfat_arch); i++) {
                    if ((machHdr->cputype == swap32(arch->cputype)) && (machHdr->cpusubtype == swap32(arch->cpusubtype))) {
                        fileoffs = swap32(arch->offset);
                        printf("[+] Correct arch is at offset %u in the file\n", fileoffs);
                        break;
                    }
                    arch++;
                }
                if (fileoffs == 0) {
                    printf("[-] Could not find correct arch in FAT image\n");
                    return; // _exit(1);
                }
            } else if (fatHdr->magic == MH_MAGIC || fatHdr->magic == MH_MAGIC_64) {
                printf("[+] Executable is a plain MACH-O image\n");
            } else {
                printf("[-] Executable is of unknown type\n");
                return; // _exit(1);
            }
            
            
            /* extract basename */
            
            tmp = strrchr(rPath, '/');
            if (tmp == NULL) {
                printf("[-] Unexpected error with filename.\n");
                return; // _exit(1);
            }
            strlcpy(npath, tmp+1, sizeof(npath));
            strlcat(npath, ".decrypted", sizeof(npath));
            strlcpy(buffer, npath, sizeof(buffer));
            
            printf("[+] Opening %s for writing.\n", npath);
            destFileHandle = open(npath, O_RDWR|O_CREAT|O_TRUNC, 0644);
            if (destFileHandle == -1) {
                static const char IOS_APPLICATIONS_FOLDER[] = "/private/var/mobile/Applications/";
                size_t appsFolderLen = strlen(IOS_APPLICATIONS_FOLDER);
                if (strncmp(IOS_APPLICATIONS_FOLDER, rPath, appsFolderLen) == 0) {
                    printf("[-] Failed opening. Most probably a sandbox issue. Trying something different.\n");
                    
                    /* create new name */
                    strlcpy(npath, IOS_APPLICATIONS_FOLDER, sizeof(npath));
                    tmp = strchr(rPath + appsFolderLen, '/');
                    if (tmp == NULL) {
                        printf("[-] Unexpected error with filename.\n");
                        return; // _exit(1);
                    }
                    tmp++;
                    *tmp++ = 0;
                    strlcat(npath, rPath+33, sizeof(npath));
                    strlcat(npath, "tmp/", sizeof(npath));
                    strlcat(npath, buffer, sizeof(npath));
                    printf("[+] Opening %s for writing.\n", npath);
                    destFileHandle = open(npath, O_RDWR|O_CREAT|O_TRUNC, 0644);
                }
                if (destFileHandle == -1) {
                    perror("[-] Failed opening");
                    printf("\n");
                    return; // _exit(1);
                }
            }
            
            /* calculate address of beginning of crypted data */
            
            bytesCount = fileoffs + encInfoCmd->cryptoff;
            
            restSize = (unsigned)lseek(srcFileHandle, 0, SEEK_END) - bytesCount - encInfoCmd->cryptsize;
            lseek(srcFileHandle, 0, SEEK_SET);
            
            printf("[+] Copying the not encrypted start of the file\n");
            
            /* first, copy all the data before the encrypted data */
            
            if (!macho_copyDataFromSrcFileToDestFile(srcFileHandle, destFileHandle, buffer, bytesCount)) {
                return;
            }
//            while (n > 0) {
//                bytesToRead = (n > sizeof(buffer)) ? sizeof(buffer) : n;
//                res = (int)read(sourceFileHandle, buffer, bytesToRead);
//                if (res != bytesToRead) {
//                    printf("[-] Error reading file\n");
//                    return; // _exit(1);
//                }
//                n -= res;
//                
//                res = (int)write(destFileHandle, buffer, bytesToRead);
//                if (res != bytesToRead) {
//                    printf("[-] Error writing file\n");
//                    return; // _exit(1);
//                }
//            }
            
            
            /* now write the previously encrypted data */
            
            printf("[+] Dumping the decrypted data into the file\n");
            res = (int)write(destFileHandle, (unsigned char *)/*pvars->*/machHdr + encInfoCmd->cryptoff, encInfoCmd->cryptsize);
            if (res != encInfoCmd->cryptsize) {
                printf("[-] Error writing file\n");
                return; // _exit(1);
            }
            
            
            /* and finish with the remainder of the file */
            
            bytesCount = restSize;
            lseek(srcFileHandle, encInfoCmd->cryptsize, SEEK_CUR);
            printf("[+] Copying the not encrypted remainder of the file\n");
            
            if (!macho_copyDataFromSrcFileToDestFile(srcFileHandle, destFileHandle, buffer, bytesCount)) {
                return;
            }
//            while (n > 0) {
//                bytesToRead = (n > sizeof(buffer)) ? sizeof(buffer) : n;
//                res = (int)read(sourceFileHandle, buffer, bytesToRead);
//                if (res != bytesToRead) {
//                    printf("[-] Error reading file\n");
//                    return; // _exit(1);
//                }
//                n -= res;
//                
//                res = (int)write(destFileHandle, buffer, bytesToRead);
//                if (res != bytesToRead) {
//                    printf("[-] Error writing file\n");
//                    return; // _exit(1);
//                }
//            }
            
            if (cryptIdOffset) {
                uint32_t zero = 0;
                cryptIdOffset += fileoffs;
                printf("[+] Setting the LC_ENCRYPTION_INFO->cryptid to 0 at offset %x\n", cryptIdOffset);
                if (    lseek(destFileHandle, cryptIdOffset, SEEK_SET) != cryptIdOffset
                    ||  write(destFileHandle, &zero, 4) != 4) {
                    printf("[-] Error writing cryptid value\n");
                }
            }
            
            printf("[+] Closing original file\n");
            close(srcFileHandle);
            printf("[+] Closing dump file\n");
            close(destFileHandle);
            
            return; //_exit(1);
        } // if cmd is enc_info(64)
        
        loadCmd = (struct load_command *)((unsigned char *)loadCmd + loadCmd->cmdsize);
    } // Load commands cycle
    
    printf("[-] This mach-o file is not encrypted. Nothing was decrypted.\n");
    
    //    _exit(1);
}

//--------------------------------------------------------------------------------------------------

//} // namespace macho
