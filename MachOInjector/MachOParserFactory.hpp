//
//  MachOParserFactory.hpp
//  MachOInjector
//
//  Created by Sergey Voronov on 7/13/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#ifndef MachOParserFactory_hpp
#define MachOParserFactory_hpp

#include <memory> // for std::
#include "IMachOParser.h"

class MachOParserFactory
{
public:
    typedef std::shared_ptr<IMachOParser> ParserPtr_t;
    
    static int Create(const uint8_t* pBuffer, bool is64Bit, ParserPtr_t& spParser);
};

#endif /* MachOParserFactory_hpp */
