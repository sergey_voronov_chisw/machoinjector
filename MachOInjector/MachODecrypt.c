//
//  MachODecrypt.c
//  MachOInjector
//
//  Created by userMacBookPro on 7/26/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

#include "MachODecrypt.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <mach-o/fat.h>
//#include <mach-o/loader.h>

//struct ProgramVars {
//    struct mach_header*	mh;
//    int*		NXArgcPtr;
//    const char***	NXArgvPtr;
//    const char***	environPtr;
//    const char**	__prognamePtr;
//};

#define swap32(value) (((value & 0xFF000000) >> 24) | ((value & 0x00FF0000) >> 8) | ((value & 0x0000FF00) << 8) | ((value & 0x000000FF) << 24) )

//--------------------------------------------------------------------------------------------------

//__attribute__((constructor))
void decrypt(const char *thisFileName, /*int argc, const char **argv, const char **envp, const char **apple, struct ProgramVars *pvars*/struct mach_header *machHdr) {

    struct load_command            *loadCmd;
    struct encryption_info_command *encInfoCmd;
    struct fat_header              *fatHdr;
    struct fat_arch                *arch;
//    struct mach_header             *machHdr;
    
    char buffer[1024];
    char rpath[4096], npath[4096]; /* should be big enough for PATH_MAX */
    unsigned int fileoffs = 0, off_cryptid = 0, restSize;
    int i, sourceFileHandle, destFileHandle, res, n, bytesToRead;
    char *tmp;
    
    printf("mach-o decryption dumper\n\n");
    
    printf("DISCLAIMER: This tool is only meant for security research purposes, not for application crackers.\n\n");
    
    
    /* detect if this is a arm64 binary */
    
    if (/*pvars->*/machHdr->magic == MH_MAGIC_64) {
        loadCmd = (struct load_command *)((unsigned char *)/*pvars->*/machHdr + sizeof(struct mach_header_64));
        printf("[+] detected 64bit ARM binary in memory.\n");
    } else { /* we might want to check for other errors here, too */
        loadCmd = (struct load_command *)((unsigned char *)/*pvars->*/machHdr + sizeof(struct mach_header));
        printf("[+] detected 32bit ARM binary in memory.\n");
    }
    
    
    /* searching all load commands for an LC_ENCRYPTION_INFO load command */
    
    for (i=0; i</*pvars->*/machHdr->ncmds; i++) {
        /*printf("Load Command (%d): %08x\n", i, lc->cmd);*/
        
        if (loadCmd->cmd == LC_ENCRYPTION_INFO || loadCmd->cmd == LC_ENCRYPTION_INFO_64) {
            encInfoCmd = (struct encryption_info_command *)loadCmd;
            
            
            /* If this load command is present, but data is not crypted then exit */
            
            if (encInfoCmd->cryptid == 0) {
                break;
            }
            off_cryptid = (/*off_t*/unsigned)((void*)&encInfoCmd->cryptid - (void*)/*pvars->*/machHdr);
            printf("[+] offset to cryptid found: @%p(from %p) = %x\n", &encInfoCmd->cryptid, /*pvars->*/machHdr, off_cryptid);
            
            printf("[+] Found encrypted data at address %08x of length %u bytes - type %u.\n",
                   encInfoCmd->cryptoff, encInfoCmd->cryptsize, encInfoCmd->cryptid);
            
            if (realpath(thisFileName/*argv[0]*/, rpath) == NULL) {
                strlcpy(rpath, thisFileName/*argv[0]*/, sizeof(rpath));
            }
            
            printf("[+] Opening %s for reading.\n", rpath);
            sourceFileHandle = open(rpath, O_RDONLY);
            if (sourceFileHandle == -1) {
                printf("[-] Failed opening.\n");
                return; // _exit(1);
            }
            
            printf("[+] Reading header\n");
            n = (int)read(sourceFileHandle, (void *)buffer, sizeof(buffer));
            if (n != sizeof(buffer)) {
                printf("[W] Warning read only %d bytes\n", n);
            }
            
            printf("[+] Detecting header type\n");
            fatHdr = (struct fat_header *)buffer;
            
            
            /* Is this a FAT file - we assume the right endianess */
            
            if (fatHdr->magic == FAT_CIGAM) {
                printf("[+] Executable is a FAT image - searching for right architecture\n");
                arch = (struct fat_arch *)&fatHdr[1];
                for (i=0; i<swap32(fatHdr->nfat_arch); i++) {
                    if ((/*pvars->*/machHdr->cputype == swap32(arch->cputype)) && (/*pvars->*/machHdr->cpusubtype == swap32(arch->cpusubtype))) {
                        fileoffs = swap32(arch->offset);
                        printf("[+] Correct arch is at offset %u in the file\n", fileoffs);
                        break;
                    }
                    arch++;
                }
                if (fileoffs == 0) {
                    printf("[-] Could not find correct arch in FAT image\n");
                    _exit(1);
                }
            } else if (fatHdr->magic == MH_MAGIC || fatHdr->magic == MH_MAGIC_64) {
                printf("[+] Executable is a plain MACH-O image\n");
            } else {
                printf("[-] Executable is of unknown type\n");
                return; // _exit(1);
            }
            
            
            /* extract basename */
            
            tmp = strrchr(rpath, '/');
            if (tmp == NULL) {
                printf("[-] Unexpected error with filename.\n");
                return; // _exit(1);
            }
            strlcpy(npath, tmp+1, sizeof(npath));
            strlcat(npath, ".decrypted", sizeof(npath));
            strlcpy(buffer, npath, sizeof(buffer));
            
            printf("[+] Opening %s for writing.\n", npath);
            destFileHandle = open(npath, O_RDWR|O_CREAT|O_TRUNC, 0644);
            if (destFileHandle == -1) {
                static const char IOS_APPLICATIONS_FOLDER[] = "/private/var/mobile/Applications/";
                size_t appsFolderLen = strlen(IOS_APPLICATIONS_FOLDER);
                if (strncmp(IOS_APPLICATIONS_FOLDER, rpath, appsFolderLen) == 0) {
                    printf("[-] Failed opening. Most probably a sandbox issue. Trying something different.\n");
                    
                    /* create new name */
                    strlcpy(npath, IOS_APPLICATIONS_FOLDER, sizeof(npath));
                    tmp = strchr(rpath + appsFolderLen, '/');
                    if (tmp == NULL) {
                        printf("[-] Unexpected error with filename.\n");
                        return; // _exit(1);
                    }
                    tmp++;
                    *tmp++ = 0;
                    strlcat(npath, rpath+33, sizeof(npath));
                    strlcat(npath, "tmp/", sizeof(npath));
                    strlcat(npath, buffer, sizeof(npath));
                    printf("[+] Opening %s for writing.\n", npath);
                    destFileHandle = open(npath, O_RDWR|O_CREAT|O_TRUNC, 0644);
                }
                if (destFileHandle == -1) {
                    perror("[-] Failed opening");
                    printf("\n");
                    return; // _exit(1);
                }
            }
            
            /* calculate address of beginning of crypted data */
            
            n = fileoffs + encInfoCmd->cryptoff;
            
            restSize = (unsigned)lseek(sourceFileHandle, 0, SEEK_END) - n - encInfoCmd->cryptsize;
            lseek(sourceFileHandle, 0, SEEK_SET);
            
            printf("[+] Copying the not encrypted start of the file\n");
            
            /* first, copy all the data before the encrypted data */
            
            while (n > 0) {
                bytesToRead = (n > sizeof(buffer)) ? sizeof(buffer) : n;
                res = (int)read(sourceFileHandle, buffer, bytesToRead);
                if (res != bytesToRead) {
                    printf("[-] Error reading file\n");
                    return; // _exit(1);
                }
                n -= res;
                
                res = (int)write(destFileHandle, buffer, bytesToRead);
                if (res != bytesToRead) {
                    printf("[-] Error writing file\n");
                    return; // _exit(1);
                }
            }
            
            
            /* now write the previously encrypted data */
            
            printf("[+] Dumping the decrypted data into the file\n");
            res = (int)write(destFileHandle, (unsigned char *)/*pvars->*/machHdr + encInfoCmd->cryptoff, encInfoCmd->cryptsize);
            if (res != encInfoCmd->cryptsize) {
                printf("[-] Error writing file\n");
                return; // _exit(1);
            }
            
            
            /* and finish with the remainder of the file */
            
            n = restSize;
            lseek(sourceFileHandle, encInfoCmd->cryptsize, SEEK_CUR);
            printf("[+] Copying the not encrypted remainder of the file\n");
            while (n > 0) {
                bytesToRead = (n > sizeof(buffer)) ? sizeof(buffer) : n;
                res = (int)read(sourceFileHandle, buffer, bytesToRead);
                if (res != bytesToRead) {
                    printf("[-] Error reading file\n");
                    return; // _exit(1);
                }
                n -= res;
                
                res = (int)write(destFileHandle, buffer, bytesToRead);
                if (res != bytesToRead) {
                    printf("[-] Error writing file\n");
                    return; // _exit(1);
                }
            }
            
            if (off_cryptid) {
                uint32_t zero=0;
                off_cryptid+=fileoffs;
                printf("[+] Setting the LC_ENCRYPTION_INFO->cryptid to 0 at offset %x\n", off_cryptid);
                if (lseek(destFileHandle, off_cryptid, SEEK_SET) != off_cryptid || write(destFileHandle, &zero, 4) != 4) {
                    printf("[-] Error writing cryptid value\n");
                }
            }
            
            printf("[+] Closing original file\n");
            close(sourceFileHandle);
            printf("[+] Closing dump file\n");
            close(destFileHandle);
            
            return; //_exit(1);
        } // if cmd is enc_info(64)
        
        loadCmd = (struct load_command *)((unsigned char *)loadCmd + loadCmd->cmdsize);
    } // Load commands cycle
    
    printf("[-] This mach-o file is not encrypted. Nothing was decrypted.\n");
    
//    _exit(1);
}

//--------------------------------------------------------------------------------------------------
/*
bool copyData(int sourceFileHandle, int destFileHandle, int bytesCount) {
    
    bool result = true;
    
    while (n > 0) {
        bytesToRead = (n > sizeof(buffer)) ? sizeof(buffer) : n;
        res = (int)read(sourceFileHandle, buffer, bytesToRead);
        if (res != bytesToRead) {
            printf("[-] Error reading file\n");
            result = false; // _exit(1);
            break;
        }
        n -= res;
        
        res = (int)write(destFileHandle, buffer, bytesToRead);
        if (res != bytesToRead) {
            printf("[-] Error writing file\n");
            result = false; // _exit(1);
            break;
        }
    }
    
    return result;
}
*/
//--------------------------------------------------------------------------------------------------
