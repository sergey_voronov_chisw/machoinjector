//
//  MachOCompose.hpp
//  MachOInjector
//
//  Created by Sergey Voronov on 7/19/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#ifndef Compose_hpp
#define Compose_hpp

#include "MachOArchData.h"
#include <sys/_types/_size_t.h>

struct MachOCompose {
    MachOCompose(void* &dest);
//    ~MachOCompose();
    void operator()(const MachOArchData *archData);
    
private:
    void *m_buffer;
};

#endif /* Compose_hpp */
