//
//  MachOUtilities.h
//  MachOInjector
//
//  Created by Sergey Voronov on 7/26/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#ifndef MachOUtilities_h
#define MachOUtilities_h

#include <_types/_uint32_t.h>
#include <mach-o/loader.h> // for mach_header

#if __cplusplus
extern "C" {
#endif
    
    uint32_t macho_read_magic       (const char *pBuf, int offset);
    int      macho_is_magic_64      (uint32_t magic);
    int      macho_should_swap_bytes(uint32_t magic);
    int      macho_is_fat           (uint32_t magic);
    void*    macho_copy_bytes       (const char *pBuf, int size, int offset);

    char*    macho_combine_patch_full_file_name(const char *patchFileName, char *machORPath);

    void     macho_decrypt          (const char *thisFileName, struct mach_header *machHdr);
    
#if __cplusplus
}   // Extern C
#endif

#endif /* MachOUtilities_h */
