//
//  main.cpp
//  MachOInjector
//
//  Created by Sergey Voronov on 7/13/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#include <iostream>
#include <errno.h>
#include "MachOFile.hpp"
#include "MachOUtilities.h"

//--------------------------------------------------------------------------------------------------

int main(int argc, const char * argv[]) {
    
    if (argc < 3) {
        
        printf("Nothing to do: not enough parameters.\n");
    } else {
        
        const char *sourceFileName = argv[1],
                   *patchFileName  = argv[2];
        try {
            
            MachOFile *machOFile = new MachOFile(sourceFileName, patchFileName);
            if (machOFile->prepare(argv[0]) != 0) {
                printf("ERROR while preparing Mach-O file.");
            } else if (!machOFile->inject()) {
                printf("ERROR while injecting into Mach-O file.");
            } else if (!machOFile->save()) {
                printf("ERROR while saving injected Mach-O file.");
            } else {
                printf("SUCCESSFULLY injected '%s'.\n"\
                       "Resulting file: '%s'\n", patchFileName, machOFile->getDestFileName());
            }
        } catch (int iErr) {
            
            printf("FAILED with '%s (%d)'\n", strerror(iErr), iErr);
        }
    }
    
    return 0;
}

//==================================================================================================
