//
//  MachOCompose.cpp
//  MachOInjector
//
//  Created by Sergey Voronov on 7/19/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#include "MachOCompose.hpp"

#include <stdlib.h> // for calloc()
#include <stdio.h>  // for printf()
#include <string>   // for memcpy()

//--------------------------------------------------------------------------------------------------

MachOCompose::MachOCompose(void* &dest)
:   m_buffer(dest) {
}

//--------------------------------------------------------------------------------------------------

//MachOCompose::~MachOCompose() {
//    if (m_buffer) {
//        free(m_buffer);
//    }
//    m_buffer = 0;
//}
//
//--------------------------------------------------------------------------------------------------

void MachOCompose::operator()(const MachOArchData *archData) {
    
    printf("Composing Mach-O...\n");
    
    try {
        
        // Write arch content
        memcpy((uint8_t *)m_buffer + archData->arch.offset, archData->macho, archData->arch.size);
        
        printf("Composed Mach-O.\n");
    } catch (int iErr) {
        
        if (iErr == -1) {
            
            printf("FAILED to compose mach-O: content out of size");
        }
    }
}

//--------------------------------------------------------------------------------------------------
