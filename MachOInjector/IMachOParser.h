//
//  IMachOParser.h
//  MachOInjector
//
//  Created by Sergey Voronov on 7/13/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#ifndef IMachOParser_h
#define IMachOParser_h

#ifndef interface
#define interface struct
#endif

interface IMachOParser {
    virtual bool IsMachOEncrypted() = 0;
    virtual bool Decrypt(const char *thisFileName) = 0;
    virtual int  MachOInjectDyLib(const char* pDynLibPath) = 0;
//    virtual int  Save(const char* pFileName) = 0;
    virtual void* getMachO() = 0;
//    virtual int  SaveToBuffer(void* &pBuffer) = 0;
};

#endif /* IMachOParser_h */
