//
//  MachOUtilities.c
//  MachOInjector
//
//  Created by Sergey Voronov on 7/26/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#include "MachOUtilities.h"
#include <mach-o/fat.h>
#include <memory.h> // for memcpy()
#include <stdlib.h> // for calloc()
#include <stdio.h>  // for sprintf()
#include <string.h> // for memcpy()
#include <errno.h>

#include <unistd.h> // for open(), lseek(), write(), _exit()
#include <fcntl.h> // for O_RDONLY, O_RDWR, O_CREAT, O_TRUNC

//--------------------------------------------------------------------------------------------------

uint32_t macho_read_magic(const char *pBuf, int offset) {
    
    uint32_t magic;
    const char *sourceBuf = pBuf + offset;
    magic = *(uint32_t *)sourceBuf;
    return magic;
}

//--------------------------------------------------------------------------------------------------

int macho_is_magic_64(uint32_t magic) {
    
    int result = 0;
    
    switch (magic) {
            
        case MH_MAGIC_64:
        case MH_CIGAM_64:
            result = 1;
            break;
            
        case MH_MAGIC:
        case MH_CIGAM:
            break;
            
        default:
            result = -1;
            break;
    }
    
    return result;
}

//--------------------------------------------------------------------------------------------------

int macho_should_swap_bytes(uint32_t magic) {
    
    return magic == MH_CIGAM || magic == MH_CIGAM_64 || magic == FAT_CIGAM;
}

//--------------------------------------------------------------------------------------------------

int macho_is_fat(uint32_t magic) {
    
    return magic == FAT_MAGIC || magic == FAT_CIGAM;
}

//--------------------------------------------------------------------------------------------------

void* macho_copy_bytes(const char *pBuf, int size, int offset) {
    
    void *result = NULL;
    memcpy(result, pBuf + offset, size);
    return result;
}

//--------------------------------------------------------------------------------------------------

char* macho_combine_patch_full_file_name(const char *patchFileName, char *machORPath) {
    
    const char *path = machORPath ? "@rpath" : "@executable_path/Frameworks";
    size_t len = strlen(path) + 1 + strlen(patchFileName);
    char *patchFileWithPath = (char *)calloc(1, len + 1);
    sprintf(patchFileWithPath, "%s/%s", path, patchFileName);
    free(machORPath);
    
    return patchFileWithPath;
}

//--------------------------------------------------------------------------------------------------

#define swap32(value) (((value & 0xFF000000) >> 24) | ((value & 0x00FF0000) >> 8) | ((value & 0x0000FF00) << 8) | ((value & 0x000000FF) << 24) )

//--------------------------------------------------------------------------------------------------

int macho_copy_data_from_src_to_dest(int srcFileHandle, int destFileHandle, char *buffer, int bytesCount) {
    
    int result = 0;
    
    int bytesToRead, bytesReadOrWritten;
    
    while (bytesCount > 0) {
        
        bytesToRead = (bytesCount > sizeof(buffer)) ? sizeof(buffer) : bytesCount;
        bytesReadOrWritten = (int)read(srcFileHandle, buffer, bytesToRead);
        if (bytesReadOrWritten != bytesToRead) {
            
            printf("[-] Error reading file\n");
            result = -1;
            break;
        }
        bytesCount -= bytesReadOrWritten;
        
        bytesReadOrWritten = (int)write(destFileHandle, buffer, bytesToRead);
        if (bytesReadOrWritten != bytesToRead) {
            
            printf("[-] Error writing file\n");
            result = -1;
            break;
        }
    }
    
    return result;
}

//--------------------------------------------------------------------------------------------------

int macho_open_dest_file(char *rPath) {
    
    int result = -1;
    
    char buffer[1024];
    char nPath[4096]; /* should be big enough for PATH_MAX */
    char *slashChar;
    
    // extract basename
    
    slashChar = strrchr(rPath, '/'); // Search for the last slash in rPath (@executable_path/)
    if (slashChar == NULL) {
        
        printf("[-] Unexpected error with filename.\n");
    } else {
        
        strlcpy(nPath, slashChar + 1, sizeof(nPath)); // nPath  = @executable_path/
        strlcat(nPath, ".decrypted", sizeof(nPath));  // nPath  = @executable_path/.decrypted
        strlcpy(buffer, nPath, sizeof(buffer));       // buffer = @executable_path/.decrypted
        
        printf("[+] Opening %s for writing.\n", nPath);
        result = open(nPath, O_RDWR|O_CREAT|O_TRUNC, 0644); // open @executable_path/.decrypted to write
        if (result == -1) {
            
            static const char IOS_APPLICATIONS_FOLDER[] = "/private/var/mobile/Applications/";
            size_t APPS_FOLDER_LEN = strlen(IOS_APPLICATIONS_FOLDER);
            if (strncmp(IOS_APPLICATIONS_FOLDER, rPath, APPS_FOLDER_LEN) == 0) {
                
                // rPath starts from /private/var/mobile/Applications/
                printf("[-] Failed opening. Most probably a sandbox issue. Trying something different.\n");
                
                // create new name
                strlcpy(nPath, IOS_APPLICATIONS_FOLDER, sizeof(nPath)); // nPath = /private/var/mobile/Applications/
                slashChar = strchr(rPath + APPS_FOLDER_LEN, '/'); // Search for the first slash in rPath
                if (slashChar == NULL) {
                    
                    printf("[-] Unexpected error with filename.\n");
                } else {
                    
                    slashChar++;
                    *slashChar++ = 0;
                    
                    strlcat(nPath, rPath + APPS_FOLDER_LEN, sizeof(nPath));
                    strlcat(nPath, "tmp/", sizeof(nPath));
                    strlcat(nPath, buffer, sizeof(nPath));
                    printf("[+] Opening %s for writing.\n", nPath);
                    result = open(nPath, O_RDWR|O_CREAT|O_TRUNC, 0644);
                }
            }
            if (result == -1) {
                
                perror("[-] Failed opening");
                printf("\n");
            }
        }
    }
    
    return result;
}

//--------------------------------------------------------------------------------------------------

int macho_handle_encryption_info(struct encryption_info_command *encInfoCmd, const char *thisFileName, struct mach_header *machHdr) {
    
    struct fat_header *fatHdr;
    struct fat_arch   *arch;
    
    char buffer[1024];
    char rPath[4096]; // should be big enough for PATH_MAX
    unsigned int archOffset = 0, cryptIdOffset, restSize;
    int i, srcFileHandle, destFileHandle, res, encryptedOffset;
    
    
    // If this load command is present, but data is not crypted then exit
    
    if (encInfoCmd->cryptid == 0) {
        
        return 0;
    }
    
    cryptIdOffset = (unsigned)((void*)&encInfoCmd->cryptid - (void*)machHdr);
    printf("[+] offset to cryptid found: @%p(from %p) = %x\n", &encInfoCmd->cryptid, machHdr, cryptIdOffset);
    
    printf("[+] Found encrypted data at address %08x of length %u bytes - type %u.\n",
           encInfoCmd->cryptoff, encInfoCmd->cryptsize, encInfoCmd->cryptid);
    
    if (realpath(thisFileName, rPath) == NULL) {
        
        strlcpy(rPath, thisFileName, sizeof(rPath));
    }
    
    printf("[+] Opening %s for reading.\n", rPath);
    srcFileHandle = open(rPath, O_RDONLY);
    if (srcFileHandle == -1) {
        
        printf("[-] Failed opening.\n");
        return 1;
    }
    
    printf("[+] Reading header\n");
    i = (int)read(srcFileHandle, (void *)buffer, sizeof(buffer));
    if (i != sizeof(buffer)) {
        
        printf("[W] Warning read only %d bytes\n", i);
    }
    
    printf("[+] Detecting header type\n");
    fatHdr = (struct fat_header *)buffer;
    
    
    // Is this a FAT file - we assume the right endianess
    
    if (fatHdr->magic == FAT_CIGAM) {
        
        printf("[+] Executable is a FAT image - searching for right architecture\n");
        arch = (struct fat_arch *)&fatHdr[1];
        for (i = 0; i < swap32(fatHdr->nfat_arch); i++) {
            
            if ((machHdr->cputype    == swap32(arch->cputype   )) &&
                (machHdr->cpusubtype == swap32(arch->cpusubtype))) {
                
                archOffset = swap32(arch->offset);
                printf("[+] Correct arch is at offset %u in the file\n", archOffset);
                break;
            }
            arch++;
        }
        if (archOffset == 0) {
            
            printf("[-] Could not find correct arch in FAT image\n");
            return 1;
        }
    } else if (fatHdr->magic == MH_MAGIC || fatHdr->magic == MH_MAGIC_64) {
        
        printf("[+] Executable is a plain MACH-O image\n");
    } else {
        
        printf("[-] Executable is of unknown type\n");
        return 1;
    }
    
    destFileHandle = macho_open_dest_file(rPath);
    if (destFileHandle < 1) {
        
        return 1;
    }
    
    
    // calculate address of beginning of crypted data
    
    encryptedOffset = archOffset + encInfoCmd->cryptoff;
    restSize = (unsigned)lseek(srcFileHandle, 0, SEEK_END) - encryptedOffset - encInfoCmd->cryptsize;
    lseek(srcFileHandle, 0, SEEK_SET);
    
    printf("[+] Copying the not encrypted start of the file\n");
    
    // first, copy all the data before the encrypted data
    
    if (macho_copy_data_from_src_to_dest(srcFileHandle, destFileHandle, buffer, encryptedOffset) != 0) {
        
        return 1;
    }
    
    
    // now write the previously encrypted data
    
    printf("[+] Dumping the decrypted data into the file\n");
    res = (int)write(destFileHandle, (unsigned char *)machHdr + encInfoCmd->cryptoff, encInfoCmd->cryptsize);
    if (res != encInfoCmd->cryptsize) {
        
        printf("[-] Error writing file\n");
        return 1;
    }
    
    
    // and finish with the reminder of the file
    
    lseek(srcFileHandle, encInfoCmd->cryptsize, SEEK_CUR);
    printf("[+] Copying the not encrypted remainder of the file\n");
    
    if (macho_copy_data_from_src_to_dest(srcFileHandle, destFileHandle, buffer, restSize) != 0) {
        
        return 1;
    }
    
    
    // Write 0 to the position of the cryptId
    
    if (cryptIdOffset) {
        
        uint32_t zero = 0;
        cryptIdOffset += archOffset;
        printf("[+] Setting the LC_ENCRYPTION_INFO->cryptid to 0 at offset %x\n", cryptIdOffset);
        if (    lseek(destFileHandle, cryptIdOffset, SEEK_SET) != cryptIdOffset
            ||  write(destFileHandle, &zero, 4) != 4) {
            
            printf("[-] Error writing cryptid value\n");
        }
    }
    
    printf("[+] Closing original file\n");
    close(srcFileHandle);
    printf("[+] Closing dump file\n");
    close(destFileHandle);
    
    return 0;
}

//--------------------------------------------------------------------------------------------------

//__attribute__((constructor))
void macho_decrypt(const char *thisFileName, struct mach_header *machHdr) {
    
    struct load_command            *loadCmd;
    struct encryption_info_command *encInfoCmd;
    int i;
    
    printf("mach-o decryption dumper\n\n");
    
    printf("DISCLAIMER: This tool is only meant for security research purposes, not for application crackers.\n\n");
    
    
    // detect if this is a arm64 binary
    
    if (machHdr->magic == MH_MAGIC_64) {
        
        loadCmd = (struct load_command *)((unsigned char *)machHdr + sizeof(struct mach_header_64));
        printf("[+] detected 64bit ARM binary in memory.\n");
    } else { // we might want to check for other errors here, too
        
        loadCmd = (struct load_command *)((unsigned char *)machHdr + sizeof(struct mach_header));
        printf("[+] detected 32bit ARM binary in memory.\n");
    }
    
    
    // searching all load commands for an LC_ENCRYPTION_INFO load command
    
    for (i = 0; i < machHdr->ncmds; i++) {
        
        // printf("Load Command (%d): %08x\n", i, loadCmd->cmd);
        
        if (loadCmd->cmd == LC_ENCRYPTION_INFO || loadCmd->cmd == LC_ENCRYPTION_INFO_64) {
            
            encInfoCmd = (struct encryption_info_command *)loadCmd;
            if (macho_handle_encryption_info(encInfoCmd, thisFileName, machHdr)) {
                
                return;
            }
        }
 
        loadCmd = (struct load_command *)((unsigned char *)loadCmd + loadCmd->cmdsize);
    }
    
    printf("[-] This mach-o file is not encrypted. Nothing was decrypted.\n");
}

//--------------------------------------------------------------------------------------------------
