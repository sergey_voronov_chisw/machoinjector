//
//  MachODecrypt.h
//  MachOInjector
//
//  Created by userMacBookPro on 7/26/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

#ifndef MachODecrypt_h
#define MachODecrypt_h

#include <mach-o/loader.h> // for mach_header

//struct ProgramVars {
//    struct mach_header*	mh;
//    int*		NXArgcPtr;
//    const char***	NXArgvPtr;
//    const char***	environPtr;
//    const char**	__prognamePtr;
//};

void decrypt(const char *thisFileName, /*int argc, const char **argv, const char **envp, const char **apple, struct ProgramVars *pvars*/struct mach_header *machHdr);
//void decrypt(const char *buffer);

#endif /* MachODecrypt_h */
