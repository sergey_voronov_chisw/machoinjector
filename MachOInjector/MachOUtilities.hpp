//
//  MachOUtilities.h
//  MachOInjector
//
//  Created by userMacBookPro on 7/18/17.
//  Copyright © 2017 Sergio. All rights reserved.
//

#ifndef MachOUtilities_h
#define MachOUtilities_h

//#include <stdio.h>
#include <stdlib.h> // for calloc()
#include <string.h> // for memcpy()
#include <_types/_uint32_t.h>
#include <mach-o/loader.h> // for mach_header

//namespace macho {

extern uint32_t macho_read_magic(const char *pBuf /*FILE *obj_file*/, int offset/* = 0*/);
int      macho_is_magic_64(uint32_t magic)/* throw(int)*/;
int      macho_should_swap_bytes(uint32_t magic);
int      macho_is_fat(uint32_t magic);
void*    macho_copy_bytes(const char *pBuf, int size, int offset/* = 0*/);
/*
template <typename T>
    T copy_bytes(const char *pBuf, int size, int offset = 0) {
    void *buf = calloc(1, size);
//    T t = nullptr;
//    const char *sourceBuf = pBuf + offset;
    memcpy(buf, pBuf + offset, size);
    return (T)buf;
}*/
    
char*    macho_combinePatchFileNameWithLocalPath(const char *patchFileName, char *machORPath);

void     macho_decrypt(const char *thisFileName, /*int argc, const char **argv, const char **envp, const char **apple, struct ProgramVars *pvars*/struct mach_header *machHdr);

//} // namespace macho

#endif /* MachOUtilities_h */
