//
//  MachOArchData.h
//  MachOInjector
//
//  Created by Sergey Voronov on 7/19/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#ifndef MachOArchData_h
#define MachOArchData_h

#include <mach-o/fat.h>

typedef
struct {
    struct fat_arch arch;
    void *macho;
} MachOArchData;

#endif /* MachOArchData_h */
