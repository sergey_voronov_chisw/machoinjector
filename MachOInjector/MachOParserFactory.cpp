//
//  MachOParserFactory.cpp
//  MachOInjector
//
//  Created by Sergey Voronov on 7/13/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#include "MachOParserFactory.hpp"
#include "MachOParser.hpp"
#include <iostream> // for Error codes

int MachOParserFactory::Create(const uint8_t* pBuffer, bool is64Bit, ParserPtr_t& spParser) {
    
    int iErr = 0;
    
    try {
        
        union {
            MachOParser<mach_header   > *p32;
            MachOParser<mach_header_64> *p64;
        } parser;
        
        if (is64Bit) {
            
            if (0 != (iErr = MachOParser<mach_header_64>::CreateInstance(pBuffer, parser.p64))) {
                
                throw iErr;
            }
            spParser.reset(parser.p64);
        } else {
            
            if (0 != (iErr = MachOParser<mach_header>::CreateInstance(pBuffer, parser.p32))) {
                
                throw iErr;
            }
            spParser.reset(parser.p32);
        }
    } catch (int err) {
        
//        if (0 != fpApp) {
//            fclose(fpApp);
//        }
        iErr = err;
    }
    
    return iErr;
}
