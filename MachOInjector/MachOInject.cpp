//
//  MachOInject.cpp
//  MachOInjector
//
//  Created by Sergey Voronov on 7/19/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#include "MachOInject.hpp"
#include <stdlib.h> // for calloc()
#include <errno.h>
#include "MachOUtilities.h" // for macho_ functions
#include "MachODecrypt.h"

//--------------------------------------------------------------------------------------------------

MachOInject::MachOInject(const char *buffer, const char *patchFileName)
:   m_buffer(buffer)
,   m_patchFileName(patchFileName)
{
}

//--------------------------------------------------------------------------------------------------
    
void MachOInject::operator()(MachOArchData *archData) {
    
    int iErr = 0;
    
    printf("MachOInjecting...\n");
    try {
        
        uint32_t magic = macho_read_magic(m_buffer + archData->arch.offset, 0);
        bool is_64 = macho_is_magic_64(magic);
        
        MachOParserFactory::ParserPtr_t spParser;
        
        if (0 != (iErr = MachOParserFactory::Create((uint8_t *)m_buffer + archData->arch.offset,
                                                    is_64,
                                                    spParser))
            ) {
            
            throw iErr;
        }
        
        if (true == spParser->IsMachOEncrypted()) {
            
            printf("'%s' is FairPlay protected, aborting.\n", "Mach-O file");
            throw EPERM;
        }
        
        if (0 != (iErr = spParser->MachOInjectDyLib(m_patchFileName))) {
            
            throw iErr;
        }
        
        archData->macho = calloc(1, archData->arch.size);
        memcpy(archData->macho, spParser->getMachO(), archData->arch.size);
        printf("MachOInjected SUCCESSFULLY!\n");
    } catch (int iErr) {
        
        printf("FAILED with '%s (%d)'\n", strerror(iErr), iErr);
    }
}

//--------------------------------------------------------------------------------------------------
