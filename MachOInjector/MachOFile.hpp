//
//  MachOFile.hpp
//  MachOInjector
//
//  Created by Sergey Voronov on 7/17/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#ifndef MachOFile_hpp
#define MachOFile_hpp

#include <mach-o/fat.h> // for fat_header
#include <vector>
#include "MachOParserFactory.hpp"
#include "MachOArchData.h"

enum MachOStatus {
    kMachOStatusNone,
    kMachOStatusPreparing,
    kMachOStatusPrepared,
    kMachOStatusMachOInjecting,
    kMachOStatusMachOInjected,
    kMachOStatusSaving,
    kMachOStatusSaved
};

class MachOFile {
public:
    MachOFile(const char *sourceFileName, const char *patchFileName);
    
    // Handling
    int  prepare(const char *thisFileName);
    bool inject();
    bool save();
    
    // Getters
    const char *getSourceFileName();
    const char *getPatchFileName();
    const char *getDestFileName();
    const MachOStatus getStatus();
    
protected:
    const char                  *m_sourceFileName;
    const char                  *m_patchFileName;
    char                         m_destFileName[260];
    char                        *m_sourceBuffer;
    MachOStatus                  m_status;
    struct fat_header           *m_fatHeader;
    bool                         m_isSwapFatData;
    std::vector<MachOArchData*>  m_vecArchData;
    
    void parseFatHeader();
    void parseBinaries();
    void setArchData(const struct fat_arch &arch, uint8_t index = 0);
};

#endif /* MachOFile_hpp */
