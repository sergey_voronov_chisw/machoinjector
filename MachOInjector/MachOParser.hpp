//
//  MachOParser.hpp
//  MachOInjector
//
//  Created by Sergey Voronov on 7/13/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#ifndef MachOParser_hpp
#define MachOParser_hpp

#include <iostream> // for Error codes and div_t
#include <mach-o/loader.h>
#include <vector>
#include <string> // for strlen(), strcpy()
#include <stdlib.h>

#include "IMachOParser.h"
#include "MachOArchData.h"
#include "MachOUtilities.h" // for combinePatchFileNameWithLocalPath(), decrypt()

//--------------------------------------------------------------------------------------------------

template< typename T >
class MachOParser : public IMachOParser
{
protected:
    typedef uint32_t RVA32;
    
    T                           *m_pMachO;
    MachOArchData                m_archData;
    load_command                *m_pCmdFirst;
    dylib_command               *m_pCmdLastLoadLib;
    RVA32                        m_uiFirstSectionRVA;
    std::vector<load_command *>  m_vecCommands;
    
    bool IsThereEnoughSpaceForCmd(const load_command *pCmd);
    int ReloadCommands();
    int Attach(const uint8_t *pBuffer);
    MachOParser(); // Prevent direct instantiation
    
public:
    virtual ~MachOParser();
    static int CreateInstance(const uint8_t *pBuffer, MachOParser* &pParser);

    // IMachOParser implementation
    bool IsMachOEncrypted();
    bool Decrypt(const char *thisFileName);
    int  MachOInjectDyLib(const char* pDynLibPath);
    void* getMachO();
    
private:
    char* ReadMachORPath();
};

//--------------------------------------------------------------------------------------------------

template< typename T >
MachOParser<T>::MachOParser()
:   m_pMachO(0)
,   m_pCmdFirst(0)
,   m_pCmdLastLoadLib(0)
,   m_uiFirstSectionRVA(UINT_MAX)
{
}

//--------------------------------------------------------------------------------------------------

template< typename T >
MachOParser<T>::~MachOParser() {
    
//    if (0 != m_pMachO)
//        free(m_pMachO);
    m_pMachO = 0;
}

//--------------------------------------------------------------------------------------------------

template< typename T >
int MachOParser<T>::CreateInstance(const uint8_t *pBuffer, MachOParser* &pParser) {
    
    if (0 == (pParser = new MachOParser<T>())) {
        
        return ENOMEM;
    }
    
    int iErr = pParser->Attach(pBuffer);
    if (0 == iErr) {
        
        return 0;
    }
    
    delete pParser;
    pParser = 0;
    
    return iErr;
}

//--------------------------------------------------------------------------------------------------

template< typename T >
void* MachOParser<T>::getMachO() {
    
    return m_pMachO;
}

//--------------------------------------------------------------------------------------------------

template< typename T >
int MachOParser<T>::ReloadCommands() {
    
    union {
        load_command       *pHdr; // Command header, common to all commands
        dylib_command      *pLoadDyLib;
        segment_command    *pSeg32;
        segment_command_64 *pSeg64;
    } cmds;
    
    union {
        section    *p32;
        section_64 *p64;
    } sections;
    sections.p32 = 0;
    
    uint8_t *pPtr = (uint8_t*)m_pCmdFirst;
    m_vecCommands.resize(m_pMachO->ncmds);
    
    for (uint32_t i = 0; i < m_pMachO->ncmds; i++, pPtr += cmds.pHdr->cmdsize) {
        
        cmds.pHdr = (load_command *)pPtr;
        m_vecCommands[i] = cmds.pHdr;
        switch (cmds.pHdr->cmd) {
                
            case LC_LOAD_DYLIB:
                m_pCmdLastLoadLib = cmds.pLoadDyLib;
                break;
                
            case LC_SEGMENT:
                *(void**)&sections = (void*)(cmds.pSeg32 + 1);
                for (uint32_t i = 0; i < cmds.pSeg32->nsects; i++) {
                    
                    if ((0 != sections.p32[i].offset) && (sections.p32[i].offset < m_uiFirstSectionRVA)) {
                        
                        m_uiFirstSectionRVA = sections.p32[i].offset;
                    }
                }
                break;
                
            case LC_SEGMENT_64:
                *(void**)&sections = (void*)(cmds.pSeg64 + 1);
                for (uint32_t i = 0; i < cmds.pSeg64->nsects; i++) {
                    
                    if ((0 != sections.p64[i].offset) && (sections.p64[i].offset < m_uiFirstSectionRVA)) {
                        
                        m_uiFirstSectionRVA = sections.p64[i].offset;
                    }
                }
                break;
        }
    }
    
    return (UINT_MAX == m_uiFirstSectionRVA) ? ENOEXEC : 0;
}

//--------------------------------------------------------------------------------------------------

template< typename T >
bool MachOParser<T>::IsThereEnoughSpaceForCmd(const load_command* pCmd) {
    
    return (m_pMachO->sizeofcmds + pCmd->cmdsize <= m_uiFirstSectionRVA);
}

//--------------------------------------------------------------------------------------------------

template< typename T >
int MachOParser<T>::Attach(const uint8_t* pBuffer) {
    
    m_pMachO = (T*)pBuffer;
    m_pCmdFirst = (load_command*)(m_pMachO + 1);
    return ReloadCommands();
}

//--------------------------------------------------------------------------------------------------

template< typename T >// Supports both the 32bit and 64 bit versions of 'mach_header'
char* MachOParser<T>::ReadMachORPath() {
    
    void *rPath = NULL;
    
    uint8_t      *pPtr = (uint8_t *)m_pCmdFirst;
    load_command *pCmd = m_pCmdFirst;
    for (uint32_t i = 0;
         i < m_pMachO->ncmds;
         i++, pPtr += pCmd->cmdsize, pCmd = (load_command *)pPtr) {
        
        if (LC_RPATH == pCmd->cmd) {
            
            struct rpath_command *rPathCmd = (rpath_command *)pCmd;
            size_t len = rPathCmd->cmdsize - rPathCmd->path.offset;
            rPath = calloc(1, len + 1);
            char *path = ((char *)rPathCmd) + rPathCmd->path.offset;
            memcpy(rPath, path, len);
            break;
        }
    }

    return (char *)rPath;
}

//--------------------------------------------------------------------------------------------------

template< typename T >// Supports both the 32bit and 64 bit versions of 'mach_header'
bool MachOParser<T>::IsMachOEncrypted() {
    
    uint8_t      *pPtr = (uint8_t *)m_pCmdFirst;
    load_command *pCmd = m_pCmdFirst;
    for (uint32_t i = 0;
         i < m_pMachO->ncmds;
         i++, pPtr += pCmd->cmdsize, pCmd = (load_command *)pPtr) {
        
        if (LC_ENCRYPTION_INFO != pCmd->cmd) {
            
            continue;
        }
        if (0 != ((encryption_info_command *)pCmd)->cryptid) {
            
            return true;
        }
        break;// We have found the encryption info section, no need to keep on searching
    }
    
    return false;
}

//--------------------------------------------------------------------------------------------------

template< typename T >// Supports both the 32bit and 64 bit versions of 'mach_header'
bool MachOParser<T>::Decrypt(const char *thisFileName) {
    
    macho_decrypt(thisFileName, NULL);

    uint8_t      *pPtr = (uint8_t *)m_pCmdFirst;
    load_command *pCmd = m_pCmdFirst;
    for (uint32_t i = 0;
         i < m_pMachO->ncmds;
         i++, pPtr += pCmd->cmdsize, pCmd = (load_command *)pPtr) {
        
        if (LC_ENCRYPTION_INFO != pCmd->cmd) {
            
            continue;
        }
        if (0 != ((encryption_info_command *)pCmd)->cryptid) {
            
            return true;
        }
        break;// We have found the encryption info section, no need to keep on searching
    }
    
    return false;
}

//--------------------------------------------------------------------------------------------------

template< typename T >// Supports both the 32bit and 64 bit versions of 'mach_header'
int MachOParser<T>::MachOInjectDyLib(const char* pDynLibPath) {
    
    char *rpath = ReadMachORPath();
    char *patchFileNameWithLocalPath = macho_combine_patch_full_file_name(pDynLibPath, rpath);
    
    union {
        dylib_command cmdMachOInjected;
        char          __pRaw__[512];
    };
    
    // Configure DYLIB command
    cmdMachOInjected.cmd                         = LC_LOAD_DYLIB;
    cmdMachOInjected.dylib.compatibility_version = 0x00010000;
    cmdMachOInjected.dylib.current_version       = 0x00020000;
    cmdMachOInjected.dylib.timestamp             = 2;
    cmdMachOInjected.dylib.name.offset           = (uint32_t)sizeof(dylib_command);
    char *pLibNameStart = (char *)(&cmdMachOInjected + 1);
    strncpy(pLibNameStart, patchFileNameWithLocalPath, sizeof(__pRaw__) - cmdMachOInjected.dylib.name.offset);
    cmdMachOInjected.cmdsize = cmdMachOInjected.dylib.name.offset + (uint32_t)strlen(pLibNameStart);
    
    // Adjust cmdsize if needed
    const size_t COMMAND_SIZE_ALIGNED_BY = 8;
    const div_t d = div((int)cmdMachOInjected.cmdsize, COMMAND_SIZE_ALIGNED_BY);
    if (0 != d.rem) { // Commands size must be aligned to COMMAND_SIZE_ALIGNED_BY
        
        memset((char *)&cmdMachOInjected + cmdMachOInjected.cmdsize, 0, COMMAND_SIZE_ALIGNED_BY - d.rem);
        cmdMachOInjected.cmdsize += (COMMAND_SIZE_ALIGNED_BY - d.rem);
    }
    
    if (FALSE == IsThereEnoughSpaceForCmd((load_command*)&cmdMachOInjected)) {
        // TBD: In case no space is available in the existing MAch-O, enlarge
        //      the size of the file and update section offsets/RVAs
        return ENOBUFS;
    }
    
    char *pMachOInjectionOffset = (char *)m_pCmdLastLoadLib + m_pCmdLastLoadLib->cmdsize;
    load_command *pLastCommand = m_vecCommands.back();
    const char *pLoadCmdsEnd = (char *)pLastCommand + pLastCommand->cmdsize;
    
    // Make space for the new command
    memmove(pMachOInjectionOffset + cmdMachOInjected.cmdsize,
            pMachOInjectionOffset,
            (size_t)(pLoadCmdsEnd - pMachOInjectionOffset));
    
    // MachOInject the dynlib command
    memcpy(pMachOInjectionOffset, &cmdMachOInjected, cmdMachOInjected.cmdsize);
    m_pMachO->ncmds++;
    m_pMachO->sizeofcmds += cmdMachOInjected.cmdsize;
    
    free(patchFileNameWithLocalPath);
    
    return 0;
}

//--------------------------------------------------------------------------------------------------

#endif /* MachOParser_hpp */
