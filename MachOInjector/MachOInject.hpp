//
//  MachOInject.hpp
//  MachOInjector
//
//  Created by Sergey Voronov on 7/19/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#ifndef MachOInject_hpp
#define MachOInject_hpp

#include <vector>
#include "MachOArchData.h"
#include "MachOParserFactory.hpp"

struct MachOInject {
    MachOInject(const char *buffer, const char *patchFileName);
    void operator()(MachOArchData *archData);
    
private:
    const char *m_buffer;
    const char *m_patchFileName;
};

#endif /* MachOInject_hpp */
