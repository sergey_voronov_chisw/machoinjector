//
//  MachOFile.cpp
//  MachOInjector
//
//  Created by Sergey Voronov on 7/17/17.
//  Copyright © 2017 CHI Software. All rights reserved.
//

#include "MachOFile.hpp"
#include "MachOUtilities.h" // for macho_ functions
#include "MachOInject.hpp"
#include "MachOCompose.hpp"

#include <stdio.h>  // for sprintf(), ftell(), fread()
#include <errno.h>  // for Error codes
#include <stdlib.h> // for malloc()

#include <mach-o/loader.h>
#include <mach-o/swap.h>

//--------------------------------------------------------------------------------------------------

MachOFile::MachOFile(const char *sourceFileName, const char *patchFileName)
:   m_sourceFileName(sourceFileName  )
,   m_patchFileName (patchFileName   )
,   m_sourceBuffer  (NULL            )
,   m_status        (kMachOStatusNone)
,   m_fatHeader     (NULL            ) {
    
    sprintf(m_destFileName, "%s.Injected", sourceFileName);
}

//--------------------------------------------------------------------------------------------------

int MachOFile::prepare(const char *thisFileName) {
    
    int iErr = 0;
    
    if (m_status != kMachOStatusNone) {
        
        return -1;
    }
    
    m_status = kMachOStatusPreparing;
    
    FILE* fpApp = fopen(m_sourceFileName, "rb");
    if (0 == fpApp) {
        
        iErr = errno;
    } else {
        
        fseek(fpApp, 0, SEEK_END);
        auto fileSize = ftell(fpApp);
        fseek(fpApp, 0, SEEK_SET);
        try {
            
            if (fileSize <= 0) {
                
                throw ENOENT;
            }
            if (0 == (m_sourceBuffer = (char*)malloc(fileSize))) {
                
                throw ENOMEM;
            }
            if (fileSize != fread(m_sourceBuffer, 1, fileSize, fpApp)) {
                
                throw errno;
            }
            fclose(fpApp);
            fpApp = 0;
            
            uint32_t magic = macho_read_magic(m_sourceBuffer, 0);
            if (macho_is_fat(magic)) {
                
                m_isSwapFatData = macho_should_swap_bytes(magic);
                parseFatHeader();
            } else {
                struct fat_arch arch = { 0, 0, 0, 0, 0 };
                arch.size = static_cast<uint32_t>(fileSize);
                setArchData(arch);
            }
            
            m_status = kMachOStatusPrepared;
        }
        catch (int err) {
            
            if (0 != fpApp) {
                
                fclose(fpApp);
            }
            iErr = err;
        }
    }

    return iErr;
}

//--------------------------------------------------------------------------------------------------

bool MachOFile::inject() {
    
    if (m_status == kMachOStatusPrepared) {
        
        m_status = kMachOStatusMachOInjecting;
        
        std::for_each(m_vecArchData.begin(),
                      m_vecArchData.end(),
                      MachOInject(m_sourceBuffer, m_patchFileName)
                      );
        m_status = kMachOStatusMachOInjected;
    }

    return m_status == kMachOStatusMachOInjected;
}

//--------------------------------------------------------------------------------------------------

bool MachOFile::save() {
    
    if (m_status == kMachOStatusMachOInjected) {
     
        m_status = kMachOStatusSaving;
        
        if (m_vecArchData.size() == 0) {
            
            printf("ERROR while saving: No architectures found.");
        } else {
            
            MachOArchData *lastArchData = m_vecArchData.back();
            const uint32_t MACHO_FILE_SIZE = lastArchData->arch.offset + lastArchData->arch.size;
            void *buffer = calloc(1, MACHO_FILE_SIZE);
            std::for_each(m_vecArchData.begin(),
                          m_vecArchData.end(),
                          MachOCompose(buffer)
                          );
            if (!buffer) {
                
                printf("ERROR while saving: No destination buffer composed.");
            } else {
                
                if (m_fatHeader) {
                    
                    // Write fat header
                    const size_t FAT_HEADER_SIZE = sizeof(struct fat_header);
                    swap_fat_header(m_fatHeader, NX_UnknownByteOrder);
                    memcpy(buffer, m_fatHeader, FAT_HEADER_SIZE);
                    
                    // Write fat archs
                    const size_t FAT_ARCH_SIZE = sizeof(struct fat_arch);
                    uint32_t fatArchOffset = FAT_HEADER_SIZE;
                    for (std::vector<MachOArchData*>::iterator
                         it = m_vecArchData.begin(); it != m_vecArchData.end(); ++it) {
                    
                        struct fat_arch arch = (*it)->arch;
                        swap_fat_arch(&arch, 1, NX_UnknownByteOrder);
                        memcpy((uint8_t *)buffer + fatArchOffset, &arch, FAT_ARCH_SIZE);
                        fatArchOffset += FAT_ARCH_SIZE;
                    }
                }
                
                // Save binary
                FILE* fpApp = fopen(m_destFileName, "wb");
                if (0 == fpApp) {
                    
                    return errno;
                }
                size_t bytesWritten = fwrite(buffer, 1, MACHO_FILE_SIZE, fpApp);
                const int iRet = (bytesWritten < MACHO_FILE_SIZE) ? errno : 0;
                fclose(fpApp);
                
                if (iRet == 0) {
                    
                    m_status = kMachOStatusSaved;
                }
            }
        }
    }
    
    return m_status == kMachOStatusSaved;
}

//--------------------------------------------------------------------------------------------------

const char *MachOFile::getSourceFileName() {
    
    return m_sourceFileName;
}

//--------------------------------------------------------------------------------------------------

const char *MachOFile::getPatchFileName() {
    
    return m_patchFileName;
}

//--------------------------------------------------------------------------------------------------

const char *MachOFile::getDestFileName() {
    
    return m_destFileName;
}

//--------------------------------------------------------------------------------------------------

void MachOFile::parseFatHeader() {
    
    const int FAT_HEADER_SIZE = sizeof(struct fat_header);
    m_fatHeader = (struct fat_header *)macho_copy_bytes(m_sourceBuffer, FAT_HEADER_SIZE, 0);
    if (m_isSwapFatData) {
        
        swap_fat_header(m_fatHeader, NX_UnknownByteOrder);
    }
    m_vecArchData.resize(m_fatHeader->nfat_arch);
    
    int fat_arch_offset = FAT_HEADER_SIZE;
    const int FAT_ARCH_SIZE = sizeof(struct fat_arch);

    for (int i = 0; i < m_fatHeader->nfat_arch; i++) {
        
        struct fat_arch *arch =
            (struct fat_arch *)macho_copy_bytes(m_sourceBuffer, FAT_ARCH_SIZE, fat_arch_offset);
        if (m_isSwapFatData) {
            
            swap_fat_arch(arch, 1, NX_UnknownByteOrder);
        }
        
        setArchData(*arch, i);
        
        free(arch);

        fat_arch_offset += FAT_ARCH_SIZE;
        
    } // arch cycle
}

//--------------------------------------------------------------------------------------------------

void MachOFile::setArchData(const struct fat_arch &arch, uint8_t index) {
    
    MachOArchData *archData = new MachOArchData( {arch} );
    if (index >= 0) {
        
        if (index < m_vecArchData.size()) {
            
            m_vecArchData[index] = archData;
        } else {
            
            m_vecArchData.push_back(archData);
        }
    } else {
        
        m_vecArchData.insert(m_vecArchData.begin(), archData);
    }
}

//--------------------------------------------------------------------------------------------------
